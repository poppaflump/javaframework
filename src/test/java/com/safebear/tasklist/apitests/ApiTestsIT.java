package com.safebear.tasklist.apitests;


import com.jayway.restassured.RestAssured;
import com.jayway.restassured.parsing.Parser;
import org.junit.Before;
import org.junit.Test;

public class ApiTestsIT {
    private final String DOMAIN = System.getProperty("domain");
    private final int PORT = Integer.parseInt(System.getProperty("port"));
    private final String CONTEXT = System.getProperty("context");

    @Before
    public void setUp(){

        RestAssured.baseURI = DOMAIN;
        RestAssured.port = PORT ;

        RestAssured.registerParser("application/json", Parser.JSON);



    }

    @Test
    public void testHomePage() {

        RestAssured.get ("/" + CONTEXT + "/api/tasks")
                .then()
                .assertThat()
                .statusCode(200);

    }

}
